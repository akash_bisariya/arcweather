package app.android.com.arcweather;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import app.android.com.weatherapplication.interfaces.IWeatherCallbackListener;
import app.android.com.weatherapplication.models.AccuWeather5DayModel;
import app.android.com.weatherapplication.models.AccuWeatherModel;
import app.android.com.weatherapplication.models.OpenWeather5DayModel;
import app.android.com.weatherapplication.models.OpenWeatherModel;
import app.android.com.weatherapplication.utils.WeatherConditions;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherDetailActivity extends AppCompatActivity implements IWeatherCallbackListener {
    @BindView(R.id.tv_weather_details_text)
    TextView tvWeatherDetailsText;
    @BindView(R.id.tv_weather_current)
    TextView tvWeatherCurrent;
    @BindView(R.id.tv_current_time)
    TextView tvCurrentTime;
    @BindView(R.id.iv_current_weather_icon)
    ImageView ivCurrentWeatherIcon;
    @BindView(R.id.tv_temperature)
    TextView tvTemperature;
    @BindView(R.id.tv_weather_condition)
    TextView tvWeatherCondition;
    @BindView(R.id.tv_temperature_max_min)
    TextView tvTemperatureMaxMin;
    @BindView(R.id.tv_temperature_location)
    TextView tvTemperatureLocation;
    @BindView(R.id.rv_5_days_weather)
    RecyclerView rv5DaysWeather;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;
    private AccuWeatherModel accuWeatherModel;
    private OpenWeatherModel openWeatherModel;
    String ACCU_WEATHER_APP_ID = "87ad516d1d4842838fcfebe843d933b1";
    private String OPEN_WEATHER_APP_ID = "b317aca2e83ad16e219ff2283ca837d5";
    String cityKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);
        ButterKnife.bind(this);

        boolean isOpenWeather = (boolean) getIntent().getExtras().get("isOpenWeather");
        rv5DaysWeather.setLayoutManager(new LinearLayoutManager(this));
        tvCurrentTime.setText(getTime() + " IST");
        if (isOpenWeather) {
            openWeatherModel = (OpenWeatherModel) getIntent().getSerializableExtra("weatherData");
            tvTemperature.setText(String.valueOf(openWeatherModel.getMain().getTemp() + " \u2103"));
            tvWeatherCondition.setText(openWeatherModel.getWeather().get(0).getDescription());

            Glide.with(WeatherDetailActivity.this)
                    .load("http://openweathermap.org/img/w/" + openWeatherModel.getWeather().get(0).getIcon() + ".png")
                    .into(ivCurrentWeatherIcon);

            pbProgress.setVisibility(View.VISIBLE);
            WeatherConditions.getOpenWeatherData5Days(openWeatherModel.getName(), OPEN_WEATHER_APP_ID, this);

        } else {
            String city = (String) getIntent().getExtras().get("city");
            String country = (String) getIntent().getExtras().get("country");
            cityKey = (String) getIntent().getExtras().get("city_key");
            accuWeatherModel = (AccuWeatherModel) getIntent().getSerializableExtra("weatherData");
            tvTemperature.setText(String.valueOf(accuWeatherModel.getTemperature().getMetric().getValue() + " \u2103"));
            tvTemperatureLocation.setText(city + " / " + country);
            tvWeatherDetailsText.setText(city + " today's weather details");
            tvWeatherCondition.setText(accuWeatherModel.getWeatherText());
            Glide.with(WeatherDetailActivity.this)
                    .load("http://apidev.accuweather.com/developers/Media/Default/WeatherIcons/" + String.format("%02d", accuWeatherModel.getWeatherIcon()) + "-s" + ".png")
                    .into(ivCurrentWeatherIcon);

            pbProgress.setVisibility(View.VISIBLE);
            WeatherConditions.getAccuWeatherData5Days(cityKey, ACCU_WEATHER_APP_ID, this, true);
        }




    }

    private String getTime() {

        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa",
                Locale.ENGLISH);
        return dateFormat.format(date);
    }

    @Override
    public void getWeatherData(Object weatherModel, Boolean success, String errorMsg) {
        pbProgress.setVisibility(View.GONE);
        if (success) {
            if (weatherModel instanceof AccuWeather5DayModel) {
                AccuWeather5DayModel accuWeather5DayModel = (AccuWeather5DayModel) weatherModel;
                rv5DaysWeather.setAdapter(new RecyclerAdapterAccuWeather(this, accuWeather5DayModel));
                tvTemperatureMaxMin.setText(String.valueOf(accuWeather5DayModel.getDailyForecasts().get(0).getTemperature().getMinimum().getValue()) +
                        "/" + String.valueOf(accuWeather5DayModel.getDailyForecasts().get(0).getTemperature().getMaximum().getValue()));


            } else if (weatherModel instanceof OpenWeather5DayModel) {
                OpenWeather5DayModel openWeather5DayModel = (OpenWeather5DayModel) weatherModel;
                try {
                    rv5DaysWeather.setAdapter(new RecyclerAdapterOpenWeather(this, openWeather5DayModel.getMinMaxTemp()));
                    Object[] keyset = openWeather5DayModel.getMinMaxTemp().keySet().toArray();
                    tvWeatherDetailsText.setText(openWeatherModel.getName() + " today's weather details");
                    tvTemperatureLocation.setText(openWeatherModel.getName() + " / " + openWeather5DayModel.getCity().getCountry());
                    tvTemperatureMaxMin.setText(String.valueOf(openWeather5DayModel.getMinMaxTemp().get(String.valueOf(keyset[0])).get(0).getTempMin()) + " / " +
                            openWeather5DayModel.getMinMaxTemp().get(String.valueOf(keyset[0])).get(openWeather5DayModel.getMinMaxTemp().get(String.valueOf(keyset[0])).size() - 1).getTempMax());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
