package app.android.com.arcweather;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import app.android.com.weatherapplication.adapters.AutoCompleteAdapter;
import app.android.com.weatherapplication.interfaces.IWeatherCallbackListener;
import app.android.com.weatherapplication.models.AccuWeatherModel;
import app.android.com.weatherapplication.models.LocationSearchModel;
import app.android.com.weatherapplication.models.OpenWeatherModel;
import app.android.com.weatherapplication.utils.WeatherConditions;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements IWeatherCallbackListener {

    @BindView(R.id.tv_accu_weather)
    TextView tvAccuWeather;
    @BindView(R.id.tv_open_weather)
    TextView tvOpenWeather;
    @BindView(R.id.tv_city)
    AutoCompleteTextView etCity;

    private LocationSearchModel mLocationSearchModel;
    private Boolean isOpenWeather=false;
    private String OPEN_WEATHER_APP_ID = "b317aca2e83ad16e219ff2283ca837d5";
    private String ACCU_WEATHER_APP_ID = "87ad516d1d4842838fcfebe843d933b1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        etCity.setThreshold(2);
        etCity.setAdapter(new AutoCompleteAdapter(this,ACCU_WEATHER_APP_ID));
        tvAccuWeather.setTextColor(getResources().getColor(R.color.c_orange));
        tvOpenWeather.setTextColor(getResources().getColor(R.color.c_gray));

        etCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mLocationSearchModel = (LocationSearchModel) adapterView.getAdapter().getItem(i);
                etCity.setText(mLocationSearchModel.getLocalizedName());
                WeatherConditions.getAccuWeatherData(mLocationSearchModel.getKey(), ACCU_WEATHER_APP_ID, MainActivity.this,true);


            }
        });
//        WeatherConditions.getOpenWeatherData("bareilly", OPEN_WEATHER_APP_ID, MainActivity.this);


        etCity.setOnEditorActionListener(new AutoCompleteTextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId== EditorInfo.IME_ACTION_DONE)
                {
                    if(isOpenWeather)
                    {
                        WeatherConditions.getOpenWeatherData(etCity.getText().toString(),OPEN_WEATHER_APP_ID,MainActivity.this);
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void getWeatherData(Object weatherModel, Boolean success, String errorMsg) {
        if (success) {
            etCity.setText("");
            Intent intent = new Intent(MainActivity.this,WeatherDetailActivity.class);
            if(weatherModel instanceof OpenWeatherModel) {
                OpenWeatherModel openWeatherModel = (OpenWeatherModel) weatherModel;
                intent.putExtra("isOpenWeather",isOpenWeather);
                intent.putExtra("weatherData",openWeatherModel);
                startActivity(intent);
            }
            else if(weatherModel instanceof AccuWeatherModel)
            {
                AccuWeatherModel accuWeatherModel = (AccuWeatherModel) weatherModel;
                intent.putExtra("city",mLocationSearchModel.getLocalizedName());
                intent.putExtra("country",mLocationSearchModel.getCountry().getLocalizedName());
                intent.putExtra("city_key",mLocationSearchModel.getKey());
                intent.putExtra("weatherData",accuWeatherModel);
                intent.putExtra("isOpenWeather",isOpenWeather);
                startActivity(intent);

            }
        }

    }

    @OnClick({R.id.tv_accu_weather, R.id.tv_open_weather, R.id.tv_city})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_accu_weather:
                tvAccuWeather.setTextColor(getResources().getColor(R.color.c_orange));
                tvOpenWeather.setTextColor(getResources().getColor(R.color.c_gray));
                isOpenWeather=false;
                etCity.setText("");
                etCity.setAdapter(new AutoCompleteAdapter(this,ACCU_WEATHER_APP_ID));
                break;
            case R.id.tv_open_weather:
                tvOpenWeather.setTextColor(getResources().getColor(R.color.c_orange));
                tvAccuWeather.setTextColor(getResources().getColor(R.color.c_gray));
                isOpenWeather=true;
                etCity.setText("");
                etCity.setAdapter(null);
                break;
            case R.id.tv_city:
                break;
        }
    }
}
