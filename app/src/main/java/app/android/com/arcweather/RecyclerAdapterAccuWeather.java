package app.android.com.arcweather;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import app.android.com.weatherapplication.models.AccuWeather5DayModel;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akash on 12/10/17.
 */

public class RecyclerAdapterAccuWeather extends RecyclerView.Adapter<RecyclerAdapterAccuWeather.RecyclerViewHolder> {
    private Context mContext;
    private List<AccuWeather5DayModel.DailyForecast> mWeatherList = new ArrayList<>();

    public RecyclerAdapterAccuWeather(Context context, AccuWeather5DayModel model) {
        this.mContext = context;
        this.mWeatherList = model.getDailyForecasts();
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_recycler, null);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {


        try {
            holder.tvDay.setText(getDay(mWeatherList.get(position).getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.tvDayTemperature.setText(String.valueOf(mWeatherList.get(position).getTemperature().getMinimum().getValue())+" / "+ String.valueOf(mWeatherList.get(position).getTemperature().getMaximum().getValue()));

        try {
            holder.tvDate.setText(setDateFormat(mWeatherList.get(position).getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvWeatherCondition.setText(mWeatherList.get(position).getDay().getIconPhrase());

        Glide.with(mContext)
                .load("http://apidev.accuweather.com/developers/Media/Default/WeatherIcons/" + String.format("%02d", mWeatherList.get(position).getDay().getIcon()) + "-s" + ".png")
                .into(holder.ivDayWeather);

    }

    private String setDateFormat(String unformattedDate) throws ParseException {
        Date dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(unformattedDate);
        return (new SimpleDateFormat("MMM dd")).format(dateformat);
    }


    private String getDay(String date) throws ParseException {
        Date dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date);
        Calendar calendar =Calendar.getInstance();
        calendar.setTime(dateformat);
        int result = calendar.get(Calendar.DAY_OF_WEEK);
        switch (result) {
            case Calendar.MONDAY:
                return "Monday";
            case Calendar.TUESDAY:
                return  "Tuesday";
            case Calendar.WEDNESDAY:
                return "Wednesday";
            case Calendar.THURSDAY:
                return "Thursday";
            case Calendar.FRIDAY:
                return "Friday";
            case Calendar.SATURDAY:
                return "Saturday";
            case Calendar.SUNDAY:
                return "Sunday";
            default:
                return "Day";
        }
    }


    @Override
    public int getItemCount() {
        return mWeatherList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_day_temperature)
        TextView tvDayTemperature;
        @BindView(R.id.tv_day_weather_condition)
        TextView tvWeatherCondition;
        @BindView(R.id.iv_day_weather)
        ImageView ivDayWeather;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
