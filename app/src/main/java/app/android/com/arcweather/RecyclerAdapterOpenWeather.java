package app.android.com.arcweather;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import app.android.com.weatherapplication.adapters.RecyclerAdapter;
import app.android.com.weatherapplication.models.AccuWeather5DayModel;
import app.android.com.weatherapplication.models.OpenWeather5DayModel;
import app.android.com.weatherapplication.models.OpenWeatherModel;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akash on 12/10/17.
 */
public class RecyclerAdapterOpenWeather extends RecyclerView.Adapter<RecyclerAdapterOpenWeather.RecyclerViewHolder> {
    private Context context;
    private Map<String, ArrayList<OpenWeather5DayModel.Main>> weatherList = new TreeMap<>();

    public RecyclerAdapterOpenWeather(Context context, Map<String, ArrayList<OpenWeather5DayModel.Main>> weatherList) {
        this.context = context;
        this.weatherList.putAll(weatherList);
    }


    @Override
    public RecyclerAdapterOpenWeather.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_recycler, null);
        return new RecyclerAdapterOpenWeather.RecyclerViewHolder(view);
    }


    private String setDateFormat(String unformattedDate) throws ParseException {
        Date dateformat = new SimpleDateFormat("yyyy-MM-dd").parse(unformattedDate);
        return (new SimpleDateFormat("MMM dd")).format(dateformat);
    }
    @Override
    public void onBindViewHolder(RecyclerAdapterOpenWeather.RecyclerViewHolder holder, int position) {

        Object[] keyset = weatherList.keySet().toArray();
        try {
            holder.tvWeatherDate.setText(setDateFormat(String.valueOf(keyset[position])));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            holder.tvWeatherDay.setText(getDay(String.valueOf(keyset[position])));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvDayTemperature.setText(String.valueOf(weatherList.get(String.valueOf(keyset[position])).get(0).getTempMin())+" / "+
                weatherList.get(String.valueOf(keyset[position])).get(weatherList.get(String.valueOf(keyset[position])).size() - 1).getTempMax());
        holder.tvWeatherCondition.setText("");
    }




    private String getDay(String date) throws ParseException {
        Date dateformat = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        Calendar calendar =Calendar.getInstance();
        calendar.setTime(dateformat);
        int result = calendar.get(Calendar.DAY_OF_WEEK);
        switch (result) {
            case Calendar.MONDAY:
                return "Monday";
            case Calendar.TUESDAY:
                return  "Tuesday";
            case Calendar.WEDNESDAY:
                return "Wednesday";
            case Calendar.THURSDAY:
                return "Thursday";
            case Calendar.FRIDAY:
                return "Friday";
            case Calendar.SATURDAY:
                return "Saturday";
            case Calendar.SUNDAY:
                return "Sunday";
            default:
                return "Day";
        }
    }


    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tvWeatherDate;
        @BindView(R.id.tv_day)
        TextView tvWeatherDay;
        @BindView(R.id.tv_day_temperature)
        TextView tvDayTemperature;
        @BindView(R.id.tv_day_weather_condition)
        TextView tvWeatherCondition;
        @BindView(R.id.iv_day_weather)
        ImageView ivDayWeather;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

