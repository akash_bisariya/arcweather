import android.app.Application;
import android.content.ComponentCallbacks;

import app.android.com.arcweather.CrashHandler;

/**
 * Created by akash on 1/11/17.
 */

public class MyApplicationClass extends Application {
    @Override
    public void registerComponentCallbacks(ComponentCallbacks callback) {
        super.registerComponentCallbacks(callback);
    }


    public void setCrashHandler()
    {
        Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(Thread.getDefaultUncaughtExceptionHandler(),MyApplicationClass.this));

    }
}
